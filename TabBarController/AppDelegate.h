//
//  AppDelegate.h
//  TabBarController
//
//  Created by Pranay Kothlapuram on 11/14/18.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

