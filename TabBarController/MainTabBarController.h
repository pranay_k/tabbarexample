//
//  MainTabBarController.h
//  TabBarController
//
//  Created by Pranay Kothlapuram on 11/14/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainTabBarController : UITabBarController

@end

NS_ASSUME_NONNULL_END
