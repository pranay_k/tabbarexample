//
//  MainTabBarController.m
//  TabBarController
//
//  Created by Pranay Kothlapuram on 11/14/18.
//

#import "MainTabBarController.h"

@interface MainTabBarController () <UITabBarControllerDelegate>

@end

@implementation MainTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.delegate = self;
}

#pragma mark - UITabBarControllerDelegate

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    if (tabBarController.selectedIndex == 1 && tabBarController.viewControllers.count > tabBarController.selectedIndex) {
        BOOL shouldShowContactVC = (BOOL)(rand() % 2);
        NSMutableArray *viewControllers = [[tabBarController viewControllers] mutableCopy];
        UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *newVC = nil;
        if (shouldShowContactVC) {
            newVC = [main instantiateViewControllerWithIdentifier:@"ContactUsVC"];
        } else {
            newVC = [main instantiateViewControllerWithIdentifier:@"AboutUsVC"];
        }
        if (newVC) {
            [viewControllers replaceObjectAtIndex:tabBarController.selectedIndex withObject:newVC];
            newVC.tabBarItem = viewController.tabBarItem;
            [tabBarController setViewControllers:viewControllers animated:YES];
        }
    }
}

@end
